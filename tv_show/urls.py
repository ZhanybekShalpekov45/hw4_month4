from django.urls import path
from . import views

urlpatterns = [
    path('tv_show/', views.films_view, name='tv_show'),
    path('tv_show/<int:id>/', views.films_detail_view, name='films_detail'),
    path('tv_show/<int:id>/delete/', views.delete_films, name='films_delete'),
    path('tv_show/<int:id>/update/', views.update_films, name='films_update'),
    path('add-tv_show/', views.create_films, name='add_films'),
]